Not much to really detail here, here's some rules I guess:

- Don't touch anything outside of `./achive`
- Nothing NSFW for now, soon though...
- Abuse of the CICD settings will end in termination of your account with us, so use it wisely
- Please make new branches for new meme collections before merging into Master
  - Anything you merge into the Master branch will be **uploaded automatically!**